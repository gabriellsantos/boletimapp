import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Platform } from 'ionic-angular';
import { BackgroundGeolocation, BackgroundGeolocationConfig, BackgroundGeolocationResponse } from '@ionic-native/background-geolocation';
import { HTTP } from '@ionic-native/http';

/**
 * Generated class for the LocalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-local',
  templateUrl: 'local.html',
})
export class LocalPage {

  url: string = "http://boletim-urna.herokuapp.com";//"http://192.168.0.108:3000";
  codigo: any;
  ativo: any;

  constructor(public http: HTTP,private _platform: Platform,private backgroundGeolocation: BackgroundGeolocation,public navCtrl: NavController, public navParams: NavParams)
  {
    this.codigo = window.localStorage.getItem("codigo") || this.navParams.get('codigo');
    this.ativo = window.localStorage.getItem("location_enable");


  }



  ionViewDidLoad() {
    console.log('ionViewDidLoad LocalPage');
  }


  startLocation()
  {

    if(this._platform.is("android"))
    {
      let config: BackgroundGeolocationConfig = {
        desiredAccuracy: 10,
        interval: 300000,//30000, //
        stationaryRadius: 20,
        distanceFilter: 30,
        notificationTitle: "Acompanhamento de Localização Ativada.",
        notificationText: 'Para desligar entre no TH Fiscal',
        debug: false, //  enable this hear sounds for background-geolocation life-cycle.
        stopOnTerminate: false, // enable this to clear background location settings when the app terminates
      };
      console.log("entrou");
      this.backgroundGeolocation.configure(config)
        .subscribe((location: BackgroundGeolocationResponse) => {

          console.log("location - "+location);
          this.sendLocation(location);
          // IMPORTANT:  You must execute the finish method here to inform the native plugin that you're finished,
          // and the background-task may be completed.  You must do this regardless if your HTTP request is successful or not.
          // IF YOU DON'T, ios will CRASH YOUR APP for spending too much time in the background.
          //this.backgroundGeolocation.finish(); // FOR IOS ONLY

        });

      // start recording location
      this.backgroundGeolocation.start();
      window.localStorage.setItem("location_enable", "1");
      this.ativo = 1;

    }


  }

  sendLocation(location)
  {
    console.log("latitude - "+location.latitude);
    this.http.get(this.url+'/partners/location?codigo='+this.codigo+"&latitude="+location.latitude+"&longitude="+location.longitude,{}, {})
      .then(data => {
        console.log("local enviado");

      })
      .catch(error => {

        console.log("erro ao enviar local");


      });
  }

  stopLocation()
  {
    this.backgroundGeolocation.stop();
    window.localStorage.setItem("location_enable", "0");
    this.ativo = 0;

  }
}
