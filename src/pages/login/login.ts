import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HTTP } from '@ionic-native/http';
import { ListPage } from '../list/list';
import { LoadingController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import {FiscalPublicoPage} from "../fiscal-publico/fiscal-publico";


/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})


export class LoginPage {

  codigo: any = "";
  nome: any = "";
  telefone: any = "";
  tipo: any;
  url: any = "http://boletim-urna.herokuapp.com";
  idFiscal: any;
  constructor(public modalCtrl: ModalController,public alertCtrl: AlertController,public loadingCtrl: LoadingController,private http: HTTP,public navCtrl: NavController, public navParams: NavParams)
  {
    this.codigo = window.localStorage.getItem("codigo");
    this.idFiscal = window.localStorage.getItem("idFiscal");
    if(window.localStorage.getItem("versao-cadastro") == "1")
    {
      this.goHome();
    }
  }

  verificaFiscal()
  {
    let loader = this.loadingCtrl.create({
      content: "Aguarde. Verificando cadastro..."
    });
    loader.present();
    this.http.get(this.url+'/partners/verificar_fiscal', {codigo: this.codigo}, {})
      .then(data =>
      {

        //console.log("data - "+JSON.parse(data.data).ok);
        if(JSON.parse(data.data).ok == true)
        {
          this.goHome();
          loader.dismiss();
        }
        else {
          loader.dismiss();
          this.showAlert("Erro","Fiscal #"+this.codigo+". Não foi possível autorizar o dispositivo! Refaça seu cadastro!")
        }


      }).catch(error => {
      loader.dismiss();

      //this.showAlert("Erro","Não foi possível enviar os dados. Verfique sua conexão com a internet!");

    });
  }

  verificaFiscalPublico()
  {
    let loader = this.loadingCtrl.create({
      content: "Aguarde. Verificando cadastro..."
    });
    loader.present();
    this.http.get(this.url+'/partners/verificar_fiscal_publico', {idFiscal: this.idFiscal}, {})
      .then(data =>
      {

        //console.log("data - "+JSON.parse(data.data).ok);
        if(JSON.parse(data.data).ok == true)
        {
          this.goHome();
          loader.dismiss();
        }
        else {
          loader.dismiss();
          this.showAlert("Erro","Fiscal #"+this.codigo+". Não foi possível autorizar o dispositivo! Refaça seu cadastro!")
        }


      }).catch(error => {
      loader.dismiss();

      this.showAlert("Erro","Não foi possível enviar os dados. Verfique sua conexão com a internet!");

    });
  }

  fiscalPublicoModal(tipo)
  {
    const modal = this.modalCtrl.create(FiscalPublicoPage,{tipo: tipo});
    modal.present();
  }

  mascara(event)
  {
    console.log("Tecla "+ event.key);
    if(event.key != "Backspace")
    {
    if(this.telefone.length == 0)
    {
      this.telefone += "(";
    }
    if(this.telefone.length == 3)
    {
      this.telefone += ")";
    }
    }
  }

  showAlert(titulo,msg) {
    let alert = this.alertCtrl.create({
      title: titulo,
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }

  goHome()
  {
    this.navCtrl.setRoot(ListPage,{tipo: this.tipo});
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');


  }

  ativar(){
    if(this.nome.trim() == "" || this.telefone.trim() == "" || this.codigo.trim() == "" )
    {
      this.showAlert("Campos Obrigatórios", "Informe todos os campos!");
      return;
    }
    let loader = this.loadingCtrl.create({
      content: "Aguarde...",
      duration: 3000
    });
    loader.present();
    this.http.get(this.url+'/partners/ativar_aparelho', {codigo: this.codigo, nome: this.nome, telefone: this.telefone}, {})
    .then(data =>
    {

      //console.log("data - "+JSON.parse(data.data).ok);
        if(JSON.parse(data.data).ok == true)
        {
          console.log("CODIGO - ",JSON.parse(data.data).codigo);
          window.localStorage.setItem("codigo",JSON.parse(data.data).codigo);
          window.localStorage.setItem("partner",JSON.parse(data.data).partner);
          window.localStorage.setItem("partner_taghere",JSON.parse(data.data).partner_taghere);
          this.goHome();
          loader.dismiss();
        }
        else {
          loader.dismiss();
          this.showAlert("Erro","Não foi possível autorizar o dispositivo!")
        }


    }).catch(error => {
      loader.dismiss();

      this.showAlert("Erro","Não foi possível enviar os dados. Verfique sua conexão com a internet!");

    });
  }
}
