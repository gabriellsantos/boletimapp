import { Component } from '@angular/core';
import { NavController, NavParams,Platform,AlertController } from 'ionic-angular';
import { OneSignal } from '@ionic-native/onesignal';
import {HomePage} from "../home/home"
import {NotificaPage} from "../notifica/notifica"
import { ScanPage } from '../scan/scan';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { HTTP } from '@ionic-native/http';
import { LoadingController } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { ToastController } from 'ionic-angular';
import {TutorialPage} from "../tutorial/tutorial";
import {SelecionaLocalPage} from "../seleciona-local/seleciona-local";
import { Geofence } from '@ionic-native/geofence';
import { BackgroundGeolocation, BackgroundGeolocationConfig, BackgroundGeolocationResponse } from '@ionic-native/background-geolocation';
import {ApuracaoPage} from "../apuracao/apuracao";
import {LocalPage} from "../local/local";
import {TicketsPage} from "../tickets/tickets";

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {
  selectedItem: any;
  icons: string[];
  items: Array<{title: string, note: string, icon: string}>;
  codigo: any;
  partner: any;
  partner_taghere: any;
  dataBase: any;
  dadosEnvio: any = [];
  urlManual: any = "";
  tipo: any;
  idFiscal: any;
  pollplace: any;

  url: string = "http://boletim-urna.herokuapp.com";//"http://192.168.0.142:3000";

  public appId: string = "9040aa87-8001-4a25-a80b-9dbc42b38e3f";//"AAAAD3oGMJ4:APA91bHQLW5ccXGl_at0IY0vrvu_3NXuQIruHdxuOBw0D40f_oK_uKD3A7oYm8OR4gXXW0xpnOrwyrq1qHOaSZK-CMVgAYF-5c9x8si4_wvHaz0vlM3mKuFslccgHJxf3I71DoukIgCg";
  public googleProjectId: string = "272457111549";

  plataforma: any;

  constructor(private backgroundGeolocation: BackgroundGeolocation,private geofence: Geofence,public toastCtrl: ToastController,public iab: InAppBrowser,public loadingCtrl: LoadingController,public http: HTTP,public sqlite: SQLite,public alertCtrl: AlertController,public navCtrl: NavController, public navParams: NavParams,public _OneSignal: OneSignal, private _platform: Platform)
  {

    /*window.localStorage.setItem("idFiscal", "12");
    window.localStorage.setItem("tipo", "fiscal");
    window.localStorage.setItem("codigo", "729829");*/

    this.idFiscal = window.localStorage.getItem("idFiscal");
    this.tipo = navParams.get('tipo') || window.localStorage.getItem("tipo") ;
    this.codigo = window.localStorage.getItem("codigo") || this.navParams.get('codigo');
    this.partner = window.localStorage.getItem("partner");
    this.partner_taghere = window.localStorage.getItem("partner_taghere");
    // If we navigated to this page, we will have an item available as a nav param
    this.selectedItem = navParams.get('item');
    this.pollplace = JSON.parse(window.localStorage.getItem("polling_place")) || JSON.parse(navParams.get("polling_place"));

    if(this._platform.is("android"))
    {
      this.plataforma = true;
    }
    else {
      this.plataforma = false;
    }


    console.log("latitude - "+this.pollplace.lat);
    console.log("longitude - "+this.pollplace.lng);

    geofence.initialize().then(
      // resolved promise does not return a value
      () => console.log('Geofence Plugin Ready'),
      (err) => console.log(err)
    )

    if(this.tipo == "fiscal")
    {
      if(window.localStorage.getItem("polling_place") == null)
      {
        this.setPollingPlace();
      }
      else {
        if(this.plataforma == true)
        {
          this.addGeofence();
        }
      }
    }


    this.openDb();
    this.verificar_local();
    this.setOneSignal();
    //this.startLocation();

    if(this._platform.is("android") && this.tipo == "fiscal")
    {
      let redirecionar_geofence = window.localStorage.getItem("geofence");
      if (redirecionar_geofence == "1") {
        this.goToLocal();
      }
    }


  }

  openios()
  {
    this.iab.create("http://urna.taghere.com.br/home/index2",'_system');
  }

  goToTickets()
  {
    this.navCtrl.push(TicketsPage);
  }

  goToLocal()
  {
    window.localStorage.setItem("geofence", "2");
    this.navCtrl.push(LocalPage);
  }

  apuracao()
  {
    this.navCtrl.push(ApuracaoPage);
  }

  verificar_local()
  {
    if(this.tipo == "fiscal")
    {
      this.http.get(this.url+'/partners/retornar_local?fiscal='+this.idFiscal,{}, {})
        .then(data => {
          console.log("local - "+JSON.parse(data.data));
          window.localStorage.setItem("polling_place", data.data);
          this.pollplace = JSON.parse(data.data);
        })
        .catch(error => {

          console.log("erro ao enviar local");


        });
    }
  }

  startLocation()
  {

    if(this._platform.is("android"))
    {
      let config: BackgroundGeolocationConfig = {
        desiredAccuracy: 10,
        interval: 30000, //300000,
        stationaryRadius: 20,
        distanceFilter: 30,
        debug: true, //  enable this hear sounds for background-geolocation life-cycle.
        stopOnTerminate: false, // enable this to clear background location settings when the app terminates
      };
      console.log("entrou");
      this.backgroundGeolocation.configure(config)
        .subscribe((location: BackgroundGeolocationResponse) => {

          console.log("location - "+location);
          this.sendLocation(location);
          // IMPORTANT:  You must execute the finish method here to inform the native plugin that you're finished,
          // and the background-task may be completed.  You must do this regardless if your HTTP request is successful or not.
          // IF YOU DON'T, ios will CRASH YOUR APP for spending too much time in the background.
          //this.backgroundGeolocation.finish(); // FOR IOS ONLY

        });

      // start recording location
      this.backgroundGeolocation.start();
    }


  }


  sendLocation(location)
  {
    console.log("latitude - "+location.latitude);
    this.http.get(this.url+'/partners/location?codigo='+this.codigo+"&latitude="+location.latitude+"&longitude="+location.longitude,{}, {})
      .then(data => {
        console.log("local enviado");

      })
      .catch(error => {

        console.log("erro ao enviar local");


      });
  }


  private addGeofence() {
    //options describing geofence
    let fence_enter = {
      id: "729829", //any unique ID this.codigo
      latitude:       this.pollplace.lat, //center of geofence radius
      longitude:      this.pollplace.lng,
      radius:         100, //radius to edge of geofence in meters
      transitionType: 1, //see 'Transition Types' below
      notification: { //notification settings
        id:             1, //any unique ID
        title:          'Local de Votação', //notification title
        text:           'Você chegou ao local de votação.', //notification body
        openAppOnClick: true //open app when notification is tapped
      }
    }

    let fence_exit = {
      id: "729828", //any unique ID this.codigo
      latitude:       this.pollplace.lat, //center of geofence radius
      longitude:      this.pollplace.lng,
      radius:         100, //radius to edge of geofence in meters
      transitionType: 2, //see 'Transition Types' below
      notification: { //notification settings
        id:             2, //any unique ID
        title:          'Local de Votação', //notification title
        text:           'Você saiu do local de votação.', //notification body
        openAppOnClick: true //open app when notification is tapped
      }
    }

    this.geofence.addOrUpdate([fence_enter,fence_exit]).then(
      () => console.log('Geofence added'),
      (err) => console.log('Geofence failed to add')
    );

    /*this.geofence.onNotificationClicked = function (notificationData) {
      console.log('App opened from Geo Notification!', notificationData);
      window.localStorage.setItem("geofence", "1");
    };*/
  }


  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000
    });
    toast.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad homePage');

  }



  goToHome(){
    this.navCtrl.setRoot(HomePage);
  }

  goToNotifica(){
    this.navCtrl.push(NotificaPage);
  }

  goToTutorial() {
    this.navCtrl.push(TutorialPage);
  }

    showAlert(titulo,msg) {
    let alert = this.alertCtrl.create({
      title: titulo,
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }

  public scan()
  {
    this.navCtrl.setRoot(ScanPage,{atual: 1, atualizar: 0})
  }





  public openDb()
  {

    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    }).then((db: SQLiteObject) => {

      db.executeSql('create table IF NOT EXISTS boletins(id INTEGER primary key, urna TEXT, qtd TEXT, dados TEXT, total_qr INTEGER, lidos INTEGER)', {})
        .then(() => console.log('Executed SQL'))
        .catch(e => console.log(e));
      db.executeSql('create  unique index if not exists idx_boletins_dados on boletins (dados)', {})
        .then(() => console.log('Executed SQL'))
        .catch(e => console.log(e));


      db.executeSql("select * from boletins where lidos = total_qr",{}).then((result) =>
      {
        console.log("DADOS - "+result.rows.length);
        if(result.rows.length > 0)
        {
          for(var i =0; i< result.rows.length;i++)
          {

            this.dadosEnvio.push(result.rows.item(i).dados);

          }
          this.httpGet();

        }
        else
        {
          //this.showAlert("Boletins", "Sem Boletins para envio! Verifique se todos os QRCODES foram lidos!");
        }
      }).catch(e => console.log(e));


    }).catch(e => console.log(e));
  }

  public httpGet()
  {
    let loader = this.loadingCtrl.create({
      content: "Aguarde. Sincronizando Dados."
    });
    loader.present();
    this.http.get('http://boletim-urna.herokuapp.com/offices/receber', {data: this.dadosEnvio, tipo: this.codigo, idFiscal: this.idFiscal}, {})
      .then(data => {

        loader.dismiss();
        this.presentToast("Dados Sincronizados com Sucesso!")


      })
      .catch(error => {

        loader.dismiss();
        this.presentToast("Não foi possível enviar os dados. Verfique sua conexão com a internet!");


      });
  }

  setOneSignal()
  {
    this._platform.ready().then(() =>
    {
      if (this._platform.is('android') || this._platform.is('ios'))
      {

        this._OneSignal.startInit(this.appId, this.googleProjectId);
        this._OneSignal.inFocusDisplaying(this._OneSignal.OSInFocusDisplayOption.Notification);
        this._OneSignal.setSubscription(true);
        this._OneSignal.handleNotificationReceived().subscribe(() => {
          // handle received here how you wish.
        });
        this._OneSignal.handleNotificationOpened().subscribe((data) => {
          // handle opened here how you wish.

          this.showAlert(data.notification.payload.title,data.notification.payload.body);
        });


        this._OneSignal.endInit();
        let tag = {};
        tag["apuracao_partner_"+this.partner] = "enable";
        tag["partner_"+this.partner_taghere] = "enable"

        this._OneSignal.sendTags(tag);
      }
    })
  }

  setPollingPlace(){
    this.navCtrl.setRoot(SelecionaLocalPage, {codigo: this.codigo})
  }


}
