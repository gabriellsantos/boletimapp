import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HTTP } from '@ionic-native/http';

/**
 * Generated class for the TicketsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tickets',
  templateUrl: 'tickets.html',
})
export class TicketsPage {

  url: string = "http://boletim-urna.herokuapp.com";//"http://192.168.0.108:3000";
  idFiscal: any;
  tickets: any = [];
  constructor(public http: HTTP,public navCtrl: NavController, public navParams: NavParams)
  {
    this.idFiscal = window.localStorage.getItem("idFiscal");
    this.getTickets();
  }

  getTickets()
  {
    this.http.get(this.url+'/fiscal_publics/'+this.idFiscal+'/retornar_tickets',{}, {})
      .then(data => {
        console.log("Tickets Recebidos");
        this.tickets = JSON.parse(data.data);

      })
      .catch(error => {

        console.log("erro ao enviar local");


      });
  }



  ionViewDidLoad() {
    console.log('ionViewDidLoad TicketsPage');
  }

}
