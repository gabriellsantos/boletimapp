import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FiscalPublicoPage } from './fiscal-publico';

@NgModule({
  declarations: [
    FiscalPublicoPage,
  ],
  imports: [
    IonicPageModule.forChild(FiscalPublicoPage),
  ],
})
export class FiscalPublicoPageModule {}
