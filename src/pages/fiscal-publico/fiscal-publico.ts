import {ChangeDetectorRef, Component} from '@angular/core';
import {IonicPage, NavController, NavParams, Platform,ViewController} from 'ionic-angular';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { HTTP } from '@ionic-native/http';
import { ListPage } from '../list/list';
import { LoadingController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import {SelecionaLocalPage} from "../seleciona-local/seleciona-local";


declare var cordova;



/**
 * Generated class for the FiscalPublicoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-fiscal-publico',
  templateUrl: 'fiscal-publico.html',
})
export class FiscalPublicoPage {
  name: any = "";
  phone: any = "";
  email: any = "";
  codigo: any;
  codigo_p: any;
  url: any = "http://boletim-urna.herokuapp.com";
  items: Observable<any[]>;
  tipo: any;
  partner: any = 0;
  partners: any;
  metodo: any = 0;
  termos: any = false;
  step: number= 1;
  code: string;
  locais: any;
  secaoSelecionada: any;
  localSelecionado: any;
  secoes: any;
  cities: any;

  public verificationId: string;

  public recaptchaVerifier:firebase.auth.RecaptchaVerifier;


  constructor(
    public iab: InAppBrowser,
    private afAuth: AngularFireAuth,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private http: HTTP,
    afDB: AngularFireDatabase,
    public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform,
    public change: ChangeDetectorRef,public viewCtrl: ViewController
  )
  {
    //this.items = afDB.list('notificato-seplan').valueChanges();

    this.tipo = navParams.get('tipo');
    if(this.tipo == "fiscal")
    {
      this.buscaParceiros();
    }
  }

  openTermos()
  {
    this.iab.create(this.url+"/partners/termos_condicoes",'_system');
  }


  sms(tipo)
  {
    this.metodo = tipo;
    const appVerifier = this.recaptchaVerifier;
    const phoneNumberString = "+55"+this.phone.replace("(","").replace(")","").replace("-","") ;//+ this.phone;


    console.log("telefone - "+phoneNumberString);
    firebase.auth().signInWithPhoneNumber(phoneNumberString, appVerifier)
      .then( confirmationResult => {
        // SMS sent. Prompt user to type the code from the message, then sign the
        // user in with confirmationResult.confirm(code).
        console.log(JSON.stringify(confirmationResult));
        this.confirmar(confirmationResult);
      })
      .catch(function (error) {
        console.error("SMS not sent", error);
      });
  }

  confirmar(confirmationResult)
  {
    let prompt = this.alertCtrl.create({
      title: 'Enter the Confirmation code',
      inputs: [{ name: 'confirmationCode', placeholder: 'Confirmation Code' }],
      buttons: [
        { text: 'Cancel',
          handler: data => { console.log('Cancel clicked'); }
        },
        { text: 'Send',
          handler: data => {
            // Here we need to handle the confirmation code
            this.confirmarUsuario(confirmationResult,data);
          }
        }
      ]
    });
    prompt.present();
  }

  confirmarUsuario(confirmationResult,data)
  {
    confirmationResult.confirm(data.confirmationCode)
      .then(result => {
        // User signed in successfully.
        console.log("Usuario -  "+JSON.stringify(result.user));
        if(this.tipo == 2) {
          this.ativarColicacao();
        }
        else{
          this.ativar();
        }
        // ...
      }).catch(function (error) {
      // User couldn't sign in (bad verification code?)
      // ...
    });
  }

  buscaParceiros()
  {
    let loader = this.loadingCtrl.create({
      content: "Aguarde..."
    });
    loader.present();
    this.http.get(this.url+'/partners/retornaParceiros.json',{}, {})
      .then(data =>
      {
        let dados = JSON.parse(data.data);
        console.log("data - ", dados);
        this.partners = dados.parceiros;
        this.cities= dados.cities;
        loader.dismiss();

      }).catch(error => {
      loader.dismiss();

      this.showAlert("Erro","Não foi possível obter as coligações!");

    });
  }

  goHome()
  {
    this.navCtrl.setRoot(ListPage,{tipo: this.tipo, codigo: this.codigo_p});
  }

  showAlert(titulo,msg) {
    let alert = this.alertCtrl.create({
      title: titulo,
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }

  ativarColicacao(){
    if(this.name.trim() == "" || this.phone.trim() == "" || this.partner.trim() == "" )
    {
      this.showAlert("Campos Obrigatórios ", "Informe todos os campos!");
      return;
    }
    let loader = this.loadingCtrl.create({
      content: "Aguarde..."
    });

    loader.present();
    this.http.get(this.url+'/partners/ativar_fiscal', {nome: this.name, telefone: this.phone, partner: this.partner}, {})
      .then(data =>
      {

        console.log("data - "+JSON.parse(data.data));
        if(JSON.parse(data.data).ok == true)
        {
          console.log("CODIGO - ",JSON.parse(data.data).codigo);
          this.codigo_p = JSON.parse(data.data).codigo;
          window.localStorage.setItem("codigo",JSON.parse(data.data).codigo);
          window.localStorage.setItem("partner",JSON.parse(data.data).partner);
          window.localStorage.setItem("partner_taghere",JSON.parse(data.data).partner_taghere);
          window.localStorage.setItem("idFiscal",JSON.parse(data.data).idFiscal);
          window.localStorage.setItem("tipo","fiscal");
          window.localStorage.setItem("versao-cadastro","1");
          this.tipo = "fiscal";
          this.navCtrl.setRoot(SelecionaLocalPage,{tipo: this.tipo, polling_place:'{\"id\":1052,\"name\":\"COLÉGIO ULBRA PALMAS - PALMAS\",\"location\":\"COLÉGIO ULBRA PALMAS - PALMAS, PALMAS, Tocantins, Brasil\",\"city_id\":89,\"zone_id\":32,\"lat\":\"-10.1812808\",\"lng\":\"-48.3145207\",\"sections_count\":14,\"fiscal_phone_count\":3,\"fiscal_phone_in_count\":0,\"status\":\"incompleta\",\"created_at\":\"2018-06-09T15:29:04.797Z\",\"updated_at\":\"2018-06-21T22:35:44.652Z\",\"address\":\"108 NORTE AL-16 AI-10, PALMAS, Tocantins, Brasil\",\"number\":null}', codigo: this.codigo_p});
          loader.dismiss();
        }
        else {
          loader.dismiss();
          this.showAlert("Erro","Não foi possível autorizar o dispositivo!")
        }


      })
  }


  ativar()
  {
    let loader = this.loadingCtrl.create({
      content: "Aguarde..."
    });
    loader.present();
    this.http.get(this.url+"/fiscal_publics/autorizar", {name: this.name, phone: this.phone, email: this.email}, {})
      .then(data =>
      {

        //console.log("data - "+JSON.parse(data.data).ok);
        if(JSON.parse(data.data).msg == "ok")
        {

          let fiscal = JSON.parse(data.data).codigo;
          let idFiscal = JSON.parse(data.data).fiscal;

          window.localStorage.setItem("polling_place", '{"id":1052,"name":"COLÉGIO ULBRA PALMAS - PALMAS","location":"COLÉGIO ULBRA PALMAS - PALMAS, PALMAS, Tocantins, Brasil","city_id":89,"zone_id":32,"lat":"-10.1812808","lng":"-48.3145207","sections_count":14,"fiscal_phone_count":3,"fiscal_phone_in_count":0,"status":"incompleta","created_at":"2018-06-09T15:29:04.797Z","updated_at":"2018-06-21T22:35:44.652Z","address":"108 NORTE AL-16 AI-10, PALMAS, Tocantins, Brasil","number":null}');
          window.localStorage.setItem("codigo",fiscal);
          window.localStorage.setItem("idFiscal",idFiscal);
          window.localStorage.setItem("tipo","publico");
          window.localStorage.setItem("versao-cadastro","1");
          this.tipo = "publico";
          //this.sms();

          this.navCtrl.setRoot(ListPage,{tipo: this.tipo, polling_place:'{"id":1052,"name":"COLÉGIO ULBRA PALMAS - PALMAS","location":"COLÉGIO ULBRA PALMAS - PALMAS, PALMAS, Tocantins, Brasil","city_id":89,"zone_id":32,"lat":"-10.1812808","lng":"-48.3145207","sections_count":14,"fiscal_phone_count":3,"fiscal_phone_in_count":0,"status":"incompleta","created_at":"2018-06-09T15:29:04.797Z","updated_at":"2018-06-21T22:35:44.652Z","address":"108 NORTE AL-16 AI-10, PALMAS, Tocantins, Brasil","number":null}', codigo: fiscal});
          loader.dismiss();
        }
        else {
          loader.dismiss();
          this.showAlert("Erro","Não foi possível autorizar o dispositivo!")
        }


      }).catch(error => {
      loader.dismiss();

      this.showAlert("Erro","Não foi possível enviar os dados. Verfique sua conexão com a internet!");

    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FiscalPublicoPage');
    this.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
  }

  sendSMS(){
    if( (this.name == "" || this.phone == "" || this.partner == 0) && this.tipo != "publico" )
    {
      this.showAlert("Campos Obrigatórios ", "Informe todos os campos!");
      return;
    }

    if( (this.name == "" || this.phone == "" || this.email == "") && this.tipo == "publico" )
    {
      this.showAlert("Campos Obrigatórios ", "Informe todos os campos!");
      return;
    }
    var that = this;
    var phone = "+55"+this.phone.replace("(","").replace(")","").replace("-","") ;
    if(this.platform.is("ios")){
      (<any>window).FirebasePlugin.getVerificationID(phone,function(id) {
        console.log("verificationID: "+id);
        that.verificationId = id;
        that.requestCode();
      }, function(error) {
        console.error(error);
      });
    }else{
      (<any>window).FirebasePlugin.verifyPhoneNumber(phone, 60, function(credential) {
        console.log(credential);
        that.verificationId = credential.verificationId;
        that.requestCode();
      }, function(error) {
        console.error(error);
      });
    }

  }

  requestCode(){
    this.step = 2;
    this.change.detectChanges();
  }

  validCode(){
    var signInCredential = firebase.auth.PhoneAuthProvider.credential(this.verificationId, this.code);
    this.afAuth.auth.signInAndRetrieveDataWithCredential(signInCredential).then(()=>{
      if(this.tipo == "fiscal") {
        console.log("coligação");
        this.ativarColicacao();
      }
      else{
        console.log(this.tipo);
        this.ativar();
      }
    }, ()=>{
      this.backStep();
    })
  }

  backStep(){
    this.step = 1;
    this.showAlert("SMS","Verifique seus dados e tente novamente");
    this.change.detectChanges();
  }


}
