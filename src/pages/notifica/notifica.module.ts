import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NotificaPage } from './notifica';

@NgModule({
  declarations: [
    NotificaPage,
  ],
  imports: [
    IonicPageModule.forChild(NotificaPage),
  ],
})
export class NotificaPageModule {}
