import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { HTTP } from '@ionic-native/http';
import { ListPage } from '../list/list';
import { LoadingController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';


/**
 * Generated class for the NotificaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-notifica',
  templateUrl: 'notifica.html',
})
export class NotificaPage {

  options: any;
  base64Image: any;
  mensagem: any;
  codigo: any;
  permitido: boolean = false;
  url: string = "http://boletim-urna.herokuapp.com"; //"http://boletim-urna.herokuapp.com"

  constructor(public alertCtrl: AlertController,public loadingCtrl: LoadingController,private http: HTTP,private camera: Camera,public navCtrl: NavController, public navParams: NavParams)
  {

      this.options = {
        quality: 100,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE
      };
      this.codigo = window.localStorage.getItem("codigo");

      this.verificarDelegado();
  }

  showAlert(titulo,msg) {
    let alert = this.alertCtrl.create({
      title: titulo,
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NotificaPage');
  }

  takePicture()
  {
    this.camera.getPicture(this.options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      this.base64Image = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
      // Handle error
    });
  }


  verificarDelegado()
  {
    console.log("verificando");
    this.http.get(this.url+'/partners/isDelegado', {fiscal: window.localStorage.getItem("idFiscal")}, {})
      .then(data =>
      {

        //console.log("data - "+JSON.parse(data.data).ok);
        console.log("verificando"+JSON.parse(data.data).delegado);
        if(JSON.parse(data.data).delegado == true)
        {

          this.permitido = true;

        }
        else {
          this.permitido = false;
        }


      }).catch(error => {
        console.log(error);
    });
  }


  enviar()
  {
    let loader = this.loadingCtrl.create({
      content: "Aguarde..."
    });
    loader.present();
    this.http.post('http://boletim-urna.herokuapp.com/notifications/receber', {codigo: this.codigo,msg: this.mensagem,base64: this.base64Image}, {})
      .then(data =>
      {

        //console.log("data - "+JSON.parse(data.data).ok);
        if(JSON.parse(data.data).retorno == "ok")
        {

          loader.dismiss();
          this.showAlert("Enviado","Envio realizado!");
          this.mensagem = "";
          this.base64Image = null;

        }
        else {
          loader.dismiss();
          this.showAlert("Erro","Não foi possível enviar a mensagem!")
        }


      }).catch(error => {
      loader.dismiss();

      this.showAlert("Erro","Não foi possível enviar os dados. Verfique sua conexão com a internet!");

    });
  }
}
