import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Platform } from 'ionic-angular';
import {DomSanitizer, SafeResourceUrl} from "@angular/platform-browser";
import { InAppBrowser } from '@ionic-native/in-app-browser';

/**
 * Generated class for the ApuracaoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-apuracao',
  templateUrl: 'apuracao.html',
})
export class ApuracaoPage {

  url: string = "http://urna.taghere.com.br/home/index2";
  public url_service: SafeResourceUrl;
  plataforma: any;
  constructor(public iab: InAppBrowser, private _platform: Platform,private domSanitizer: DomSanitizer,public navCtrl: NavController, public navParams: NavParams)
  {
    this.url_service = this.domSanitizer.bypassSecurityTrustResourceUrl(this.url);
    if(this._platform.is("android"))
    {
      this.plataforma = true;
    }
    else {
      this.plataforma = false;
    }
  }

  openios()
  {
    this.iab.create(this.url,'_system');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ApuracaoPage');
  }

}
