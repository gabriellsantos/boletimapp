import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ApuracaoPage } from './apuracao';

@NgModule({
  declarations: [
    ApuracaoPage,
  ],
  imports: [
    IonicPageModule.forChild(ApuracaoPage),
  ],
})
export class ApuracaoPageModule {}
