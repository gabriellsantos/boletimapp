import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { BoletimPage } from '../boletim/boletim';
import { AlertController } from 'ionic-angular';
import {ListPage} from "../list/list";


/**
 * Generated class for the ScanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-scan',
  templateUrl: 'scan.html',
})
export class ScanPage {

  scanSub: any;
  dataBase: any;
  qrCode: any = "teste 2";
  passos: any = 0;
  atual: any = 1;
  urna: any;
  atualizar: any = 0;

  constructor(
    public alertCtrl: AlertController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public qrScanner: QRScanner,
    public toastCtrl: ToastController,
    private sqlite: SQLite)
  {
    this.atual = navParams.get('atual');
    this.atualizar = navParams.get('atualizar');

    this.openDb();

    this.scan();
  }

  showAlert(titulo,msg) {
    let alert = this.alertCtrl.create({
      title: titulo,
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }

  public scan()
  {
    this.showCamera();
    this.qrScanner.prepare()
      .then((status: QRScannerStatus) => {
        if (status.authorized) {
          console.log('Camera Permission Given');
          this.scanSub = this.qrScanner.scan().subscribe((text: string) => {
            this.inserirBoletim(text);
            this.qrScanner.hide();
            this.scanSub.unsubscribe();
          });
          this.qrScanner.show();
        } else if (status.denied) {
          console.log('Camera permission denied');
        } else {
          console.log('Permission denied for this runtime.');
        }
      })
      .catch((e: any) => console.log('Error is', e));
  }

  public openDb()
  {
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    }).then((db: SQLiteObject) =>
    {
      this.dataBase = db;

    }).catch(e => console.log(e));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ScanPage');
  }

  inserirBoletim(dados)
  {


    let splitedText = dados.split(" ");

    let qtd = splitedText[0].slice(5);

    if(this.atual == 1 && qtd.split(":")[0] != 1 )
    {
      this.showAlert("Atenção","Leia primeiro o QRCODE 1 de 1!");
      return;
    }

    if(this.atual != qtd.split(":")[0]  )
    {
      this.showAlert("Atenção","Leia os QRCODE em ordem!");
      return;
    }

    this.passos = qtd.split(":")[1];
    this.atual = qtd.split(":")[0];

    for(var i = 0; i < splitedText.length; i++)
    {
      if(qtd.split(":")[0] == "1")
      {
        if(splitedText[i].split(":")[0] == "IDUE")//Pega o número da urna
        {
          this.urna = splitedText[i].split(":")[1];
        }
      }

    }

    if(this.atual == 1)
    {

      this.qrCode = dados;
      this.presentToast("dados lidos")
    }
    else
    {

      this.qrCode = " "+dados;

    }

    this.salvar();

  }

  salvar()
  {
    let id = this.atualizar;
    if(this.atualizar == 0)
    {
      this.dataBase.executeSql('insert or replace INTO boletins (urna, qtd , dados , total_qr, lidos) values(?,?,?,?,1)',[this.urna,this.passos,this.qrCode,this.passos]).then((result) =>
      {
        console.log("Dados Inseridos no dispositivo!");
        this.redirecionar(result.insertId);

      }).catch(e => console.log(e));
    }
    else
    {
      this.dataBase.executeSql('update boletins set dados = dados||?, lidos = lidos + 1 where id = ?',[this.qrCode,this.atualizar]).then((result) =>
      {
        console.log("Dados Atualizados no dispositivo!");
        this.redirecionar(id);

      }).catch(e => console.log(e));

    }




  }

  redirecionar(id)
  {
    console.log("ID - "+id);
    this.navCtrl.setRoot(ListPage);
    this.navCtrl.push(BoletimPage,{boletim: id});
  }

  ionViewWillEnter(){



  }

  ionViewWillLeave(){
    this.hideCamera();
  }



  showCamera() {
    (window.document.querySelector('ion-app') as HTMLElement).classList.add('cameraView');
  }

  hideCamera() {
    (window.document.querySelector('ion-app') as HTMLElement).classList.remove('cameraView');
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000
    });
    toast.present();
  }

  goToHome(){
    this.navCtrl.setRoot(ListPage);
  }


}
