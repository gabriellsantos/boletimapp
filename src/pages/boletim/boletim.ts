import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { ScanPage } from '../scan/scan';
import { AlertController } from 'ionic-angular';
import { HTTP } from '@ionic-native/http';
import { LoadingController } from 'ionic-angular';

/**
 * Generated class for the BoletimPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-boletim',
  templateUrl: 'boletim.html',
})
export class BoletimPage {

  boletim: any = [];
  boletim_id: any;
  visualizar: boolean = false;
  codigo: any;
  idFiscal: any;
  constructor(public loadingCtrl: LoadingController,private http: HTTP,public alertCtrl: AlertController,public navCtrl: NavController, public navParams: NavParams,private sqlite: SQLite)
  {
    this.boletim_id = navParams.get('boletim');
    this.idFiscal = window.localStorage.getItem("idFiscal");
    this.codigo = window.localStorage.getItem("codigo");
    console.log("BOLETIM - "+this.boletim_id);
    this.openDb();
  }

  showAlert(titulo,msg) {
    let alert = this.alertCtrl.create({
      title: titulo,
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BoletimPage');
  }

  public ocultar()
  {
    console.log("Visualizar -"+this.visualizar);
    this.visualizar = !this.visualizar;
    console.log("Visualizar 2 -"+this.visualizar);
  }



  public scan(item)
  {

    this.navCtrl.pop();
    this.navCtrl.push(ScanPage,{atual: item.lidos+1, atualizar: item.id});
  }


  public openDb()
  {
    this.boletim = [];
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    }).then((db: SQLiteObject) => {


      db.executeSql("select * from boletins where id = ?",[this.boletim_id]).then((result) =>
      {



        if(result.rows.length > 0)
        {
          for(var i =0; i< result.rows.length;i++)
          {

            this.boletim.push({id: result.rows.item(i).id,urna: result.rows.item(i).urna, qtd: result.rows.item(i).qtd, dados: result.rows.item(i).dados, total_qr: result.rows.item(i).total_qr, lidos: result.rows.item(i).lidos});

          }
          if(this.boletim[0].lidos != this.boletim[0].total_qr)
          {
            this.showAlert("Ler Boletim","Faltam QRCODES para serem lidos!");
          }
          else
          {
             this.httpGet();
          }
        }
        else
        {

        }
      }).catch(e => console.log(e));



    }).catch(e => console.log(e));
  }

  public httpGet()
  {
    let loader = this.loadingCtrl.create({
      content: "Aguarde. Sincronizando Dados."
    });
    loader.present();

    this.http.get('http://boletim-urna.herokuapp.com/offices/receber', {data: [this.boletim[0].dados], tipo: this.codigo, idFiscal: this.idFiscal}, {})
      .then(data => {

        loader.dismiss();
        this.showAlert("Concluído","Dados Sincronizados com Sucesso!");

      })
      .catch(error => {

        loader.dismiss();
        this.showAlert("Erro","Não foi possível enviar os dados. Verfique sua conexão com a internet!");

      });
  }

  scanMore()
  {
    this.navCtrl.push(ScanPage,{atual: 1, atualizar: 0})
  }
}
