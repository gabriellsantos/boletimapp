import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelecionaLocalPage } from './seleciona-local';

@NgModule({
  declarations: [
    SelecionaLocalPage,
  ],
  imports: [
    IonicPageModule.forChild(SelecionaLocalPage),
  ],
})
export class SelecionaLocalPageModule {}
