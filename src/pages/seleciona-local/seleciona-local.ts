import { Component } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HTTP } from '@ionic-native/http';
import {AlertController, IonicPage, LoadingController, NavController, NavParams, Platform} from 'ionic-angular';
import { SelectSearchableComponent } from 'ionic-select-searchable';

import {ListPage} from "../list/list";
/**
 * Generated class for the SelecionaLocalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-seleciona-local',
  templateUrl: 'seleciona-local.html',
})
export class SelecionaLocalPage {
  cities: any=[];
  city: any;

  places: any= [];
  place: any;
  url: any = "http://boletim-urna.herokuapp.com";////"http://192.168.0.108:3000";
  selected: boolean= false;
  place_id: number;
  code: string;
  search: string="";
  step: number=0;
  zona: number;
  secao: number;



  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public loadingCtrl: LoadingController,
              private http: HTTP,
              public alertCtrl: AlertController,
              public platform: Platform
  ) {

    this.platform.ready().then(() => {
      this.code = navParams.get('codigo') || window.localStorage.getItem("codigo") ;
      let loader = this.loadingCtrl.create({
        content: "Aguarde..."
      });
      loader.present();
      this.http.get(this.url + '/partners/retornaParceiros.json', {}, {})
        .then(data => {
          let dados = JSON.parse(data.data);
          console.log("data - ", dados);
          this.cities = dados.cities;
          loader.dismiss();

        }).catch(error => {
        loader.dismiss();

        this.showAlert("Erro", error);

      });
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SelecionaLocalPage');
  }

  cityChange(event: { component: SelectSearchableComponent, value: any }) {
    console.log('city:', event.value);
    let loader = this.loadingCtrl.create({
      content: "Aguarde..."
    });
    loader.present();
    this.http.get(this.url + '/polling_places/by_city?city='+ event.value.id, {}, {})
      .then(data => {
        let dados = JSON.parse(data.data);
        console.log("data - ", dados);
        this.places = dados.places;
        this.zona = dados.zone;
        this.search= "";
        this.step = 1;
        loader.dismiss();

      }).catch(error => {
      loader.dismiss();

      this.showAlert("Erro", error);

    });
  }

  placeChange(event: { component: SelectSearchableComponent, value: any }) {
    console.log('place:', event.value);
    this.selected = true;
    this.place_id = event.value.id
  }

  selectPlace(){
    let loader = this.loadingCtrl.create({
      content: "Aguarde..."
    });
    this.http.get(this.url + '/polling_places/'+this.place_id+'/set_phone?codigo='+ this.code, {}, {})
      .then(data => {
        let dados = JSON.parse(data.data);
        console.log("data - ", dados);
        window.localStorage.setItem("polling_place", data.data);
        this.navCtrl.setRoot(ListPage,{polling_place: data.data });
        loader.dismiss();
      }).catch(error => {
      loader.dismiss();
      console.log(error);
      this.showAlert("Erro", "Temos um problema, tente novamente mais tarde.");

    });
  }

  selecionaTipo(tipo){
    this.step = 2;
    this.search = tipo;
  }

  voltar(){
    this.step = 1;
    this.search ="";
  }




  selectPlaceZona(){
    let loader = this.loadingCtrl.create({
      content: "Aguarde..."
    });
    this.http.get(this.url+'/partners/set_polling_place?codigo='+this.code+"&zona="+this.zona+"&secao="+this.secao, {}, {})
      .then(data => {
        let dados = JSON.parse(data.data);
        window.localStorage.setItem("polling_place", data.data);
        this.navCtrl.setRoot(ListPage,{polling_place: data.data });
        console.log("loooo - "+JSON.parse(data.data).lat);
        loader.dismiss();
      }).catch(error => {
      loader.dismiss();
      console.log(error);
      this.showAlert("Erro", "Temos um problema, tente novamente mais tarde.");

    });
  }

  showAlert(titulo,msg) {
    let alert = this.alertCtrl.create({
      title: titulo,
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }

}
