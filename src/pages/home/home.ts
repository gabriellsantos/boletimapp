import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner';
import { ScanPage } from '../scan/scan';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { BoletimPage } from '../boletim/boletim';
import { AlertController } from 'ionic-angular';
import { HTTP } from '@ionic-native/http';
import { LoadingController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  dataBase: any = "";
  boletins: any = [];
  dadosEnvio: any = [];
  constructor(public loadingCtrl: LoadingController,private http: HTTP,public alertCtrl: AlertController,public navCtrl: NavController,public qrScanner: QRScanner,private sqlite: SQLite) {
    //this.openDb();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad homePage');
    //this.openDb();
  }

  showAlert(titulo,msg) {
    let alert = this.alertCtrl.create({
      title: titulo,
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }


  ionViewDidLeave() {
    window.document.querySelector('ion-app').classList.remove('transparentBody')
  }


  public httpGet()
  {
    let loader = this.loadingCtrl.create({
      content: "Aguarde..."
    });
    loader.present();
    this.http.get('http://boletim-urna.herokuapp.com/offices/receber', {data: this.dadosEnvio}, {})
      .then(data => {

        loader.dismiss();
        this.showAlert("Envio","Envio concluído");

      })
      .catch(error => {

        loader.dismiss();
        this.showAlert("Erro","Não foi possível enviar os dados. Verfique sua conexão com a internet!");

      });
  }

  public enviar()
  {
    this.dataBase.executeSql("select * from boletins where lidos = total_qr",{}).then((result) =>
    {

      if(result.rows.length > 0)
      {
        for(var i =0; i< result.rows.length;i++)
        {

          this.dadosEnvio.push(result.rows.item(i).dados);

        }
        this.httpGet();

      }
      else
      {
          this.showAlert("Boletins", "Sem Boletins para envio! Verifique se todos os QRCODES foram lidos!");
      }
    }).catch(e => console.log(e));
  }


  ionViewWillEnter(){
    console.log('Voltando homePage');
    this.openDb();
  }

  public scan()
  {
    this.navCtrl.push(ScanPage,{atual: 1, atualizar: 0})
  }

  visualizarBoletim(id)
  {
    this.navCtrl.push(BoletimPage,{boletim: id});
  }

  public openDb()
  {
    this.boletins = [];
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    }).then((db: SQLiteObject) => {

      /*db.executeSql('drop table boletins', {})
        .then(() => console.log('Drop Orders'))
        .catch(e => console.log(e));*/




      db.executeSql('create table IF NOT EXISTS boletins(id INTEGER primary key, urna TEXT, qtd TEXT, dados TEXT, total_qr INTEGER, lidos INTEGER)', {})
        .then(() => console.log('Executed SQL'))
        .catch(e => console.log(e));
      db.executeSql('create  unique index if not exists idx_boletins_dados on boletins (dados)', {})
        .then(() => console.log('Executed SQL'))
        .catch(e => console.log(e));

      this.dataBase = db;

      db.executeSql("select * from boletins",{}).then((result) =>
      {

        console.log("tamanho result = "+ result.rows.length);

        if(result.rows.length > 0)
        {
          for(var i =0; i< result.rows.length;i++)
          {

            this.boletins.push({id: result.rows.item(i).id,urna: result.rows.item(i).urna, qtd: result.rows.item(i).qtd, dados: result.rows.item(i).dados, total_qr: result.rows.item(i).total_qr, lidos: result.rows.item(i).lidos});

          }

        }
        else
        {

        }
      }).catch(e => console.log(e));



    }).catch(e => console.log(e));
  }

}
