import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HTTP } from '@ionic-native/http';
import { LoadingController } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';

/**
 * Generated class for the TutorialPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tutorial',
  templateUrl: 'tutorial.html',
})
export class TutorialPage {

  codigo: any;
  urlManual: any = "";
  urlVideo: any = "";
  constructor(public iab: InAppBrowser,public loadingCtrl: LoadingController,public http: HTTP,public navCtrl: NavController, public navParams: NavParams)
  {
    this.codigo = window.localStorage.getItem("codigo");

    this.getManual();
    this.getVideo();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TutorialPage');
  }

  openPdf(url)
  {
    this.iab.create("http://boletim-urna.herokuapp.com/"+url,'_system');
  }

  public manual()
  {

    this.iab.create(this.urlManual,'_system');
  }


  public video()
  {
    this.iab.create(this.urlVideo,'_system');
  }


  public getManual()
  {
    console.log("GETMANUAL - "+this.codigo);
    this.http.get('http://boletim-urna.herokuapp.com/partners/manual', {codigo: this.codigo}, {})
      .then(data => {

        this.urlManual = JSON.parse(data.data).url;
        console.log("URL - "+this.urlManual);
      })
      .catch(error => {

      });

  }

  public getVideo()
  {

    this.http.get('http://boletim-urna.herokuapp.com/partners/youtubeVideo', {codigo: this.codigo}, {})
      .then(data => {

        this.urlVideo = JSON.parse(data.data).url;
      })
      .catch(error => {

      });

  }

}
