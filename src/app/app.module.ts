import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { ScanPage } from '../pages/scan/scan';
import { BoletimPage } from '../pages/boletim/boletim';
import { LoginPage } from '../pages/login/login';
import { NotificaPage } from '../pages/notifica/notifica';
import {FiscalPublicoPage} from "../pages/fiscal-publico/fiscal-publico";

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Camera } from '@ionic-native/camera';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { HTTP } from '@ionic-native/http';
import { OneSignal } from '@ionic-native/onesignal';

import { Pro } from '@ionic/pro';
import { Injectable, Injector } from '@angular/core';
import { BrMaskerModule } from 'brmasker-ionic-3';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import {TutorialPage} from '../pages/tutorial/tutorial';
import { SMS } from '@ionic-native/sms';
import { Network } from '@ionic-native/network';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule, AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { BackgroundGeolocation, BackgroundGeolocationConfig, BackgroundGeolocationResponse } from '@ionic-native/background-geolocation';
import { Geofence } from '@ionic-native/geofence';
import * as firebase from "firebase";
import {SelecionaLocalPage} from "../pages/seleciona-local/seleciona-local";
import {SelectSearchableModule} from "ionic-select-searchable";
import {ApuracaoPage} from "../pages/apuracao/apuracao";
import {LocalPage} from "../pages/local/local";
import {TicketsPage} from "../pages/tickets/tickets";


export const firebaseConfig = {
  apiKey: "AIzaSyC78wJC8Jtdri6-1XrKEuEMCh06o3q4kPU",
  authDomain: "notificato-seplan.firebaseapp.com",
  databaseURL: "https://notificato-seplan.firebaseio.com",
  projectId: "notificato-seplan",
  storageBucket: "notificato-seplan.appspot.com",
  messagingSenderId: "713265584121"
};



Pro.init('96a2278b', {
  appVersion: '0.0.15'
})




@Injectable()
export class MyErrorHandler implements ErrorHandler {
  ionicErrorHandler: IonicErrorHandler;

  constructor(injector: Injector) {
    try {
      this.ionicErrorHandler = injector.get(IonicErrorHandler);
    } catch(e) {
      // Unable to get the IonicErrorHandler provider, ensure
      // IonicErrorHandler has been added to the providers list below
    }
    firebase.initializeApp(firebaseConfig);
  }

  handleError(err: any): void {
    Pro.monitoring.handleNewError(err);
    // Remove this if you want to disable Ionic's auto exception handling
    // in development mode.
    this.ionicErrorHandler && this.ionicErrorHandler.handleError(err);
  }
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    ScanPage,
    BoletimPage,
    LoginPage,
    NotificaPage,TutorialPage, FiscalPublicoPage,
    SelecionaLocalPage,ApuracaoPage,LocalPage,TicketsPage
  ],
  imports: [
    BrowserModule,
    BrMaskerModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    SelectSearchableModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    ScanPage,
    BoletimPage,
    LoginPage,
    NotificaPage,TutorialPage,FiscalPublicoPage,
    SelecionaLocalPage,ApuracaoPage,LocalPage,TicketsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    QRScanner,
    SQLite,
    HTTP,OneSignal,IonicErrorHandler,InAppBrowser,SMS,
    Network,AngularFireDatabase,BackgroundGeolocation,Geofence,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
